/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BATTERYINFO_H
#define BATTERYINFO_H

#include <QObject>
#include <QSystemDeviceInfo>

QTM_USE_NAMESPACE

class BatteryInfo : public QObject
{
    Q_OBJECT
public:
    explicit BatteryInfo(QObject *parent = 0);

    Q_INVOKABLE int getBatteryLevel() const;
    Q_INVOKABLE bool getBatteryIsCharging();
    
signals:
    void batteryLevelChanged(int level);
    void powerStateChanged(QSystemDeviceInfo::PowerState state);
        
private:
    QSystemDeviceInfo qSystemDeviceInfo;
    
};

#endif // BATTERYINFO_H
