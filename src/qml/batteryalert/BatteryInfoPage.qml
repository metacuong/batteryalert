import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    id: page1
    tools: commonTools
    orientationLock: PageOrientation.LockPortrait

    Header {title: qsTr("Battery Information")}

    Image {
        id: imgPercent
        x: 178
        y: 103
    }

    Text {
        id: percentBattery
        x: 178
        y: 413
        width: 124
        height: 28
        text: qsTr("%1%").arg(batteryInfo.getBatteryLevel())
        color: "white"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 24
        font.bold: true
    }

    Text {
        id: textChargingState
        x: 0
        y: 491
        width: 480
        height: 47
        color: "white"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 30
    }

    Button {
        id: btTestLB
        y: 570
        text: "Voice test low battery"
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: ClientInterface.test(true)
        width: 400
    }

    Button {
        id: btTestFCB
        y: 650
        text: "Voice test battery fully charged"
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: ClientInterface.test(false)
        width: 400
    }

    Connections {
        target: Settings
        onDaemonStateChaned: daemonStateChanged()
    }

    function daemonStateChanged() {
        var dS = Settings.getDaemonState()
        btTestLB.enabled = dS
        btTestFCB.enabled = dS
    }

    function chargingState() {
        var state = qsTr("Not charging");

        if (batteryInfo.getBatteryIsCharging())
            state = qsTr("Charging")

        textChargingState.text = state
    }

    function imgPercentChange(level) {
        var cIndex = ""
        if (level <= 20)
            cIndex = "20"
        else if (level > 20 && level <= 40)
        cIndex = "40"
        else if (level > 40 && level <= 60)
        cIndex = "60"
        else if (level > 60 && level <= 80)
        cIndex = "80"
        else
            cIndex = "100"

        imgPercent.source = qsTr("images/%1.png").arg(cIndex)
    }

    Connections {
        target: batteryInfo
        onBatteryLevelChanged: {
            percentBattery.text = qsTr("%1%").arg(level)
            imgPercentChange(level)
        }

        onPowerStateChanged: {
            chargingState()
        }
    }

    Component.onCompleted: {
        chargingState()
        imgPercentChange(batteryInfo.getBatteryLevel())
        daemonStateChanged()
    }

}
