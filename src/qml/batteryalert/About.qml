import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    id: pageAbout
    tools: commonTools
    orientationLock: PageOrientation.LockPortrait

    Header {id:headerPage; title: qsTr("About")}

    Label {
        text: qsTr("<b>Battery Alert</b><br/>version: %1<br/><br/>Cuong Le<br/>metacuong@gmail.com").arg(appVersion)
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 24
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    Image {
        id: image1
        y: 190
        anchors.horizontalCenter: parent.horizontalCenter
        source: "file:///usr/share/icons/hicolor/80x80/apps/batteryalert.png"
    }

    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: pageStack.pop()
        }
    }
}
