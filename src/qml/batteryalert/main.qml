import QtQuick 1.1
import com.nokia.meego 1.0

PageStackWindow {
    id: appWindow

    initialPage: initPage

    Page {
        id: initPage
        tools: commonTools
        orientationLock: PageOrientation.LockPortrait
        TabGroup {
             id: tabgroup
             currentTab: tabBatteryAlertSetupPage
             BatteryAlertSetupPage {
                 id: tabBatteryAlertSetupPage
             }
             BatteryInfoPage {
                 id: tabBatteryInfoPage
              }
         }
    }

    Menu {
        id: myMenu
        MenuLayout {
            MenuItem { text: qsTr("About"); onClicked: openAboutPage() }
        }
    }

    ToolBarLayout {
        id: commonTools
        visible: true
        ButtonRow {
            style: TabButtonStyle { }
            TabButton {
                text: qsTr("Setup")
                tab: tabBatteryAlertSetupPage
            }
            TabButton {
                text: qsTr("Battery Info")
                tab: tabBatteryInfoPage
            }
        }
        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (myMenu.status === DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }
    }

    function openAboutPage() {
        pageStack.push(Qt.resolvedUrl("About.qml"))
    }

    Component.onCompleted: {
         theme.inverted = true
         theme.colorScheme = 6
    }
}
