import QtQuick 1.1
import com.nokia.meego 1.0
import QtMultimediaKit 1.1

Page {
    id: pageFileBox
    tools: commonTools
    orientationLock: PageOrientation.LockPortrait

    Header {id:headerPage; title: qsTr("File Browser")}

    property bool fromDirect : false
    property bool setFullyCharged: true
    property string absFilePath: ""

    Rectangle {
        id: title
        y: headerPage.height
        width: pageFileBox.width
        height: 60
        color: "#ccd7e4"

        Row {
            anchors.fill: parent
            spacing: 5

            Label {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Choose an audio file (mp3, wav)")
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pixelSize: 26
                color: "black"
                width: parent.width
            }
        }

        z:1
    }

    Rectangle {
        id:caption
        y: headerPage.height + title.height
        width: parent.width
        height: 40
        color:"#F0C5EB"
        z:1

        property alias text: caption_label.text

        Label {
            id:caption_label
            x:10
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            font.pixelSize: 20
        }
    }

    /*! --------- */

    property Component fileListDelegate: Component {
        Rectangle {
            id: recItemDel
            width: pageFileBox.width
            height: 75
            color: (!model.is_folder && model.is_selected)?"white":"transparent"

            Rectangle {
                y: recItemDel.height - 1
                height: 1
                width: recItemDel.width
                color: "grey"
                z:1
            }

            Row {
                spacing: 10
                anchors.verticalCenter: parent.verticalCenter

                Column {
                    Rectangle {
                        visible: !model.is_folder && fromDirect
                        x: 5
                       width: 5
                        height: 65
                        color: (!model.is_folder && model.is_selected)?"blue":"grey"
                    }
                }

                Column {
                    width: 69
                    Image {
                        x:5
                        width: 64
                        height: 64
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: model.is_folder?"images/folder.png":"images/file.png"
                    }
                }

                Column {
                    spacing: 3
                    Label {
                        text: model.name
                        color: "white"
                        width: recItemDel.width - 100
                        elide: Text.ElideRight
                        font.bold: model.is_folder
                        font.pixelSize: 28
                    }

                    Row {
                        spacing: 4
                        Column {
                            Text {
                                text:  fileModel.get_locale_date(model.updated)
                                color: "gray"
                                font.pixelSize: 20
                            }
                        }
                        Column {
                            Text {
                                text: !model.is_folder? bytes_exchange(model.size):""
                                font.pixelSize: 20
                            }
                        }
                    }

                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (model.is_folder)
                        fileModel.change_folder(model.name)
                    else {
                        absFilePath = model.absfilepath
                        setFile()
                        pageStack.pop()
                    }
                }
                onPressAndHold: {
                    if (!model.is_folder) {
                        absFilePath = model.absfilepath
                        previewMenu.open()
                    }
                }

                onPressed : recItemDel.color = "#E8CAD6"
                onReleased: recItemDel.color = (!model.is_folder && model.is_selected)?"white":"transparent"
                onCanceled: recItemDel.color = (!model.is_folder && model.is_selected)?"white":"transparent"
            }
        }
    }

    ListView {
        id: fileList
        visible: true
        anchors.fill: parent
        anchors.topMargin: headerPage.height + title.height + caption.height
        model: fileModel
        delegate: fileListDelegate
    }

    /*! --------- */

    function bytes_exchange(bytes) {
        var s = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        if (bytes) {
            var e = Math.floor(Math.log(bytes)/Math.log(1024));
            return (bytes/Math.pow(1024, Math.floor(e))).toFixed(2)+" "+s[e];
        }

        return bytes + " " + s[0];
    }

    //-------------- Rec for Empty Folder -----------

    Rectangle {
        id:recEmptyFolder
        visible: false
        anchors.fill: parent
        color: "transparent"
        Label {
            anchors.centerIn: parent
            text : qsTr("This folder is empty")
            font.bold: false
            width: parent.width
            font.pixelSize: 68
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            color:"#E8B7CB"
        }
    }

    //-------------- End Rec for Empty Folder ------

    Component.onCompleted: {
        fileModel.reset_all()
        fileModel.update_current_folder()
    }

    Connections {
        target: fileModel
        onFolderChanged: {
            caption.text = fileModel.get_current_folder()
            recEmptyFolder.visible = is_empty
        }
        onFolder_selected_ok: {
            btn_choose.enabled = has_selected
        }
    }

    ContextMenu {
        id:previewMenu
        MenuLayout {
            MenuItem { text: qsTr("Set it"); onClicked: setFile()}
            MenuItem { text: qsTr("Listen ..."); onClicked: listenFile()}
        }
    }

    ToolBarLayout {
        id: commonTools
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: {
                fileModel.back_to_parent_folder()
            }
        }
        Row {
            anchors.centerIn: parent
            ToolButton {
                text: qsTr("Cancel")
                onClicked: pageStack.pop()
                font.pixelSize: 22
            }
        }
    }

    Audio {
        id: playVoice
        onStopped: {
             playVoice.playing = false
            playVoice.paused = false
            playVoice.position = 0
            listenFileDlg.close()
        }
    }

    QueryDialog {
        id: listenFileDlg
        icon: "file:///usr/share/icons/hicolor/80x80/apps/batteryalert.png"
        titleText: "Battery Alert"
        message: qsTr("Sound playing ...")
        acceptButtonText: qsTr("Close")
        onAccepted: {
            playVoice.stop()
        }
    }

    function setFile() {
        if (setFullyCharged)
            Settings.setFullyChargedVoiceFilePath(absFilePath)
        else
            Settings.setLowBatteryVoiceFilePath(absFilePath)
    }

    function listenFile() {
        playVoice.source = absFilePath
        playVoice.play()
        listenFileDlg.open()
    }
}
