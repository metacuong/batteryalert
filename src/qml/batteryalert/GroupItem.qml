import QtQuick 1.1
import com.nokia.meego 1.0

Item {
    id: groupHeader
    anchors.left: parent.left
    anchors.right: parent.right
    height: titleLabel.height

    property alias title: titleLabel.text

    Image {
        anchors.left: parent.left
        anchors.right: titleLabel.left
        anchors.rightMargin: 16
        anchors.verticalCenter: parent.verticalCenter
        source: "image://theme/meegotouch-groupheader-background"
    }

    Label {
        id: titleLabel
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        font.bold: true
        font.pixelSize: 18
        color: "#8C8C8C"
    }
}
