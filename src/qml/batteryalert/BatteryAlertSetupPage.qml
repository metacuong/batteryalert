import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    id: basPage
    tools: commonTools
    orientationLock: PageOrientation.LockPortrait

    Header {id: headerPage; title: qsTr("Battery Alert Setup")}    

    property bool __completed: false

    Flickable {
        id: fickableMain

        anchors.top: headerPage.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        contentHeight: cols.height + 2*16

        clip: true
        flickableDirection: Flickable.VerticalFlick

        Column {
            id: cols

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 16
            anchors.rightMargin: 16

            spacing: 16

            Rectangle {
                id: item
                height: enabledTitleLabel.height + enabledDescriptionLabel.height + 16
                width: parent.width
                color: "transparent"
                z: 2

                Label {
                    id: enabledTitleLabel
                    anchors.top: parent.top
                    anchors.topMargin: 16
                    anchors.left: parent.left
                    anchors.right: enabledSwitch.left
                    text: "Activate battery alert"
                }

                Label {
                    id: enabledDescriptionLabel
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: enabledSwitch.left
                    color: "#808080"
                    wrapMode: Text.WordWrap
                    text: "Get voice alerts when the battery is fully charged or low battery."
                }

                Switch {
                    id: enabledSwitch
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    checked: Settings.getDaemonState()
                    onCheckedChanged: {
                        Settings.setDaemonState(checked)                        
                        ClientInterface.setDaemonState(checked)
                    }
                }
            }

            GroupItem {title: qsTr("When battery is fully charged")}

            Item {
                id:itemFullyCharged
                width: parent.width
                height: cCol.height

                Column {
                    id: cCol
                    spacing: 5
                    Label {
                        id: vAlert
                        text: qsTr("Voice alert file")
                    }

                    Row {
                        spacing: 5
                        TextField {
                            id: vAlertInput
                            font.pixelSize: 22
                            width: 390
                            text: Settings.getFullyChargedVoiceFilePath()
                        }

                        Button {
                            text: qsTr("...")
                            width: 50
                            onClicked: openFileBox(true)
                        }
                    }
                }
            }

            GroupItem {title: qsTr("When battery is low")}

            Item {
                id:itemLow
                width: parent.width
                height: lCol.height + lCol1.height

                Column {
                    spacing: 5

                    Column {
                        id: lCol1
                        spacing: 5
                        Label {
                            id: lAlert1
                            width: parent.width
                        }

                        Slider {
                            stepSize: 1
                            minimumValue: 1
                            maximumValue: 100
                            value: Settings.getLowBatteryLevelPercent()
                            valueIndicatorVisible: true
                            width: 450
                            onValueChanged: {
                                lAlert1.text = qsTr("Set low battery level <b>(%1%)</b>").arg(value)
                                if (basPage.__completed)
                                    Settings.setLowBatteryLevelPercent(value)
                            }
                        }
                    }

                    Column {
                        id: lCol
                        spacing: 5
                        Label {
                            id: lAlert
                            text: qsTr("Voice alert file")
                        }

                        Row {
                            spacing: 5
                            TextField {
                                id: lAlertInput
                                font.pixelSize: 22
                                width: 390
                                text : Settings.getLowBatteryVoiceFilePath()
                            }

                            Button {
                                text: qsTr("...")
                                width: 50
                                onClicked: openFileBox(false)
                            }
                        }
                    }

                }
            }


        }
    }

    function openFileBox(val) {
        appWindow.pageStack.push(Qt.resolvedUrl("FileBox.qml"), {setFullyCharged:val})
    }

    Connections {
        target: Settings
        onLowBatteryVoiceFilePathChanged: {
            lAlertInput.text = filePath
        }
        onFullyChargedVoiceFilePathChanged: {
            vAlertInput.text = filePath
        }
    }

    Component.onCompleted: {
        __completed = true
    }
}
