/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CLIENTINTERFACE_H
#define CLIENTINTERFACE_H

#include <QObject>
#include <QDBusMessage>
#include <QDBusConnection>

#include "global.h"

class ClientInterface : public QObject
{
    Q_OBJECT
public:
    explicit ClientInterface(QObject *parent = 0);
    
signals:
    
public slots:
    Q_INVOKABLE void setDaemonState(const bool&);
    Q_INVOKABLE void test(const bool&);

private:
    QDBusMessage methodCall(QString, QList<QVariant>) const;
    
};

#endif // CLIENTINTERFACE_H
