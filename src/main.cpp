/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QObject>
#include <QtGui/QApplication>
#include <QDeclarativeContext>
#include <QDeclarativeEngine>
#include <QDeclarativeView>
#include <applauncherd/MDeclarativeCache>

#include "settings.h"
#include "clientinterface.h"
#include "filemodel.h"
#include "batteryinfo.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication *app = MDeclarativeCache::qApplication(argc, argv);
    QDeclarativeView *view = MDeclarativeCache::qDeclarativeView();

    app->setOrganizationName(ORGANIZATION_NAME);
    app->setApplicationName(APPLICATION_NAME);
    app->setApplicationVersion(QString("%0.%1.%2").arg(APPVERSION_MAJOR).arg(APPVERSION_MINOR).arg(APPVERSION_PATCH));

     QDeclarativeContext *qDeclarativeContext = view->rootContext();
     qDeclarativeContext->setContextProperty("Settings", new Settings);
     qDeclarativeContext->setContextProperty("ClientInterface", new ClientInterface);
     qDeclarativeContext->setContextProperty("fileModel", new FileModel);
     qDeclarativeContext->setContextProperty("batteryInfo", new BatteryInfo);
     qDeclarativeContext->setContextProperty("appVersion", app->applicationVersion());

    view->setAttribute(Qt::WA_LockPortraitOrientation, true);
    view->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    view->setSource(QUrl::fromLocalFile("/opt/batteryalert/qml/batteryalert/main.qml"));
    //view->setSource(QUrl("qrc:/qml/batteryalert/main.qml"));
    view->showFullScreen();

    return app->exec();
}
