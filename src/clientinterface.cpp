/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "clientinterface.h"

ClientInterface::ClientInterface(QObject *parent) :
    QObject(parent)
{
}

void ClientInterface::setDaemonState(const bool &val) {
    methodCall("daemonState", QList<QVariant>() << QVariant::fromValue(val));
}

void ClientInterface::test(const bool &val) {
    methodCall("testVoice", QList<QVariant>() << QVariant::fromValue(val));
}

QDBusMessage ClientInterface::methodCall(QString method, QList<QVariant> args) const{
    QDBusMessage message = QDBusMessage::createMethodCall(BA_SERVICE,BA_PATH,BA_INTERFACE, method);
    message.setArguments(args);
    return QDBusConnection::sessionBus().call(message);
}
