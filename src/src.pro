include(../globals.pri)

CONFIG += qdeclarative-boostable

MOBILITY += feedback

QT += core dbus declarative

CONFIG += mobility
MOBILITY += systeminfo

TARGET = batteryalert
TEMPLATE = app

SOURCES += main.cpp \
    clientinterface.cpp \
    filemodel.cpp \
    batteryinfo.cpp

contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/batteryalert/bin
    qml.files = qml/batteryalert
    qml.path = /opt/batteryalert/qml
    desktop.files = batteryalert.desktop
    desktop.path = /usr/share/applications
    icon.files = ../batteryalert.png
    icon.path = /usr/share/icons/hicolor/80x80/apps
    splash.files = ../splash.png
    splash.path = /opt/batteryalert
    voice_samples.files = ../raw/*.mp3
    voice_samples.path = /opt/batteryalert/voice_samples
    INSTALLS += target desktop icon style splash qml voice_samples
}

INCLUDEPATH += "../global"

HEADERS += \
    clientinterface.h \
    filemodel.h \
    batteryinfo.h
