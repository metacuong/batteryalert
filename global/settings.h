/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QStringList>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);

    Q_INVOKABLE void setDaemonState(const bool&);
    Q_INVOKABLE bool getDaemonState() const;

    Q_INVOKABLE void setLowBatteryVoiceFilePath(const QString&);
    Q_INVOKABLE QString getLowBatteryVoiceFilePath() const;

    Q_INVOKABLE void setLowBatteryLevelPercent(const int&);
    Q_INVOKABLE int getLowBatteryLevelPercent() const;

    Q_INVOKABLE void setFullyChargedVoiceFilePath(const QString&);
    Q_INVOKABLE QString getFullyChargedVoiceFilePath() const;

signals:
    void daemonStateChaned();
    void lowBatteryVoiceFilePathChanged(const QString &filePath);
    void fullyChargedVoiceFilePathChanged(const QString &filePath);
    
private:
    QVariant sGet(QString, QString, const QVariant&) const;
    void sStore(QString, QString, QVariant);
    
};

#endif // SETTINGS_H
