/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "settings.h"

#define DEFAULT_DAEMON_STARTED false //not started yet
#define DEFAULT_LOW_BATTERY_VOICE_FILEPATH "/opt/batteryalert/voice_samples/low_battery_f.mp3"
#define DEFAULT_LOW_BATTERY_LEVEL_PERCENT 25 // 25%
#define DEFAULT_FULLY_CHARGED_BATTERY_VOICE_FILEPATH "/opt/batteryalert/voice_samples/battery_is_fully_charged_f.mp3"

Settings::Settings(QObject *parent) :
    QObject(parent)
{
}

void Settings::setDaemonState(const bool &val) {
    sStore("daemon", "started", val);
    emit daemonStateChaned();
}

bool Settings::getDaemonState() const {
    return sGet("daemon", "started", DEFAULT_DAEMON_STARTED).toBool();
}

void Settings::setLowBatteryVoiceFilePath(const QString &val) {
    sStore("battery", "low_battery_voice", val);
    emit lowBatteryVoiceFilePathChanged(val);
}

QString Settings::getLowBatteryVoiceFilePath() const {
    return sGet("battery", "low_battery_voice", DEFAULT_LOW_BATTERY_VOICE_FILEPATH).toString();
}

void Settings::setLowBatteryLevelPercent(const int &val) {
    sStore("battery", "low_battery_level", val);
}

int Settings::getLowBatteryLevelPercent() const {
    return sGet("battery", "low_battery_level", DEFAULT_LOW_BATTERY_LEVEL_PERCENT).toInt();
}

void Settings::setFullyChargedVoiceFilePath(const QString &val) {
    sStore("battery", "fully_charged_battery_voice", val);
    emit fullyChargedVoiceFilePathChanged(val);
}

QString Settings::getFullyChargedVoiceFilePath() const {
    return sGet("battery", "fully_charged_battery_voice", DEFAULT_FULLY_CHARGED_BATTERY_VOICE_FILEPATH).toString();
}

QVariant Settings::sGet(QString groupName, QString keyName, const QVariant &defaultVal) const {
    QSettings settings;
    settings.beginGroup(groupName);
    if (settings.childKeys().indexOf(keyName) == -1)
        return defaultVal;
    return settings.value(keyName);
}

void Settings::sStore(QString groupName, QString keyName, QVariant val) {
    QSettings settings;
    settings.setValue(QString("%1/%2").arg(groupName).arg(keyName), val);
}
