/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BATTERYHANDLER_H
#define BATTERYHANDLER_H

#include <QObject>
#include <QMediaPlayer>
#include <QDBusConnection>
#include <QSystemDeviceInfo>
#include <QtGui/QApplication>

#ifdef CONSOLE_LOGS_MODE
#include <QDebug>
#endif

#include "global.h"
#include "settings.h"

QTM_USE_NAMESPACE

class BatteryHandler : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.cuonglb.batteryalertd")
public:
    explicit BatteryHandler(QObject *parent = 0);
    ~BatteryHandler();
       
public slots:
    Q_NOREPLY void daemonState(const bool&);
    Q_NOREPLY void settingsChanged();

    Q_NOREPLY void testVoice(const bool&);

private slots:
    void powerStateChanged(const QSystemDeviceInfo::PowerState&);
    void batteryLevelChanged(const int&);

private:
    void initHandler();

    QSystemDeviceInfo *qSystemDeviceInfo;    
    QMediaPlayer *qMediaPlayer;
    QString m_low_battery_voice_filepath;
    int m_low_battery_level_percent;
    QString m_battery_fully_charged_voice_filepath;
    Settings m_settings;
    bool m_charging;
};

#endif // BATTERYHANDLER_H
