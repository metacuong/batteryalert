/*
 * Battery Alert for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "batteryhandler.h"

BatteryHandler::BatteryHandler(QObject *parent) :
    QObject(parent),
    qSystemDeviceInfo(new QSystemDeviceInfo(this)),
    qMediaPlayer(new QMediaPlayer(this))
{
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "Register object and service to session bus";
#endif
    QDBusConnection::sessionBus().registerObject(BA_OBJECT, this, QDBusConnection::ExportAllSlots | QDBusConnection::ExportAllSignals);
    QDBusConnection::sessionBus().registerService(BA_SERVICE);

    initHandler();
    settingsChanged();
}

BatteryHandler::~BatteryHandler() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "Unregister object and service to session bus";
#endif
    delete qMediaPlayer;
    delete qSystemDeviceInfo;
    QDBusConnection::sessionBus().unregisterObject(BA_OBJECT);
    QDBusConnection::sessionBus().unregisterService(BA_SERVICE);
}

void BatteryHandler::daemonState(const bool &state) {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << QString("daemonState = %1").arg(state?"Open":"Close");
#endif
    if (!state) {
        QApplication::exit(0);
    }
}

void BatteryHandler::initHandler() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "initHandler()";
#endif
    m_charging = false;
    QObject::connect(qSystemDeviceInfo, SIGNAL(batteryLevelChanged(int)), this, SLOT(batteryLevelChanged(int)));
    QObject::connect(qSystemDeviceInfo, SIGNAL(powerStateChanged(QSystemDeviceInfo::PowerState)), this, SLOT(powerStateChanged(QSystemDeviceInfo::PowerState)));
}

void BatteryHandler::batteryLevelChanged(const int &batLevel) {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << QString("batteryLevelChanged(%1)").arg(QString::number(batLevel));
#endif
    if ((batLevel == m_low_battery_level_percent) && !m_charging) {
        qMediaPlayer->setMedia(QUrl::fromLocalFile(m_low_battery_voice_filepath));
        qMediaPlayer->play();
    } else if (batLevel == 100 && m_charging) {
        qMediaPlayer->setMedia(QUrl::fromLocalFile(m_battery_fully_charged_voice_filepath));
        qMediaPlayer->play();
    }
}

void BatteryHandler::settingsChanged() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "settingsChanged()";
#endif
    m_low_battery_voice_filepath = m_settings.getLowBatteryVoiceFilePath();
    m_low_battery_level_percent = m_settings.getLowBatteryLevelPercent();
    m_battery_fully_charged_voice_filepath = m_settings.getFullyChargedVoiceFilePath();
}

void BatteryHandler::powerStateChanged(const QSystemDeviceInfo::PowerState &state) {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << QString("powerStateChanged(%1)").arg(state);
#endif
    m_charging = (state== QSystemDeviceInfo::WallPower) || (state == QSystemDeviceInfo::WallPowerChargingBattery);
}

void BatteryHandler::testVoice(const bool &val) {
    qMediaPlayer->setMedia(QUrl::fromLocalFile(val?m_low_battery_voice_filepath:m_battery_fully_charged_voice_filepath));
    qMediaPlayer->play();
}
