include(../globals.pri)

QT += core dbus
QT -= gui

CONFIG += console
CONFIG -= app_bundle
CONFIG += link_pkgconfig

CONFIG += mobility
MOBILITY += systeminfo multimedia

TARGET = batteryalertd
TEMPLATE = app

SOURCES += \
    src/main.cpp \
    src/batteryhandler.cpp

contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/batteryalert/bin
    services.files = *.service
    services.path = /usr/share/dbus-1/services
    autorun.files = *.conf
    autorun.path = /etc/init/apps
    INSTALLS += target services autorun
}

OTHER_FILES += \
    com.cuonglb.batteryalertd.service

HEADERS += \
    src/batteryhandler.h

INCLUDEPATH += "../global"

DEFINES += CONSOLE_LOGS_MODE
